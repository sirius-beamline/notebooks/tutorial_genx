# Tutorials GenX

Here you will find some old notebooks on how to use GenX (https://aglavic.github.io/genx/index.html) with data obtained on SIRIUS (Synchrotron SOLEIL).

Interested readers should instead look [here](https://arnaudhemmerle.github.io/data-analysis-on-sirius/chapters/xrr/genx/about.html) for a more detailed introduction to XRR analysis of SIRIUS data using GenX :

# Disclaimer

    Copyright (C) 2024  HEMMERLE Arnaud

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

## howto_batch
The notebook ```batch_genx.ipynb``` details the different steps on how to use GenX with command lines.
The aim is to batch fits for XRR without the GUI. To do so we edit the .hgx files, which are just hdf5 files.

The script ```batch_genx.py``` is a condensed script without all the details from the notebook.

## howto_parameters
Explains what are the different parameters in the GUI.

## examples
Temporary, should be redone in the future.
